package ua.i0xhex.srvrestart.manager;

import java.time.*;
import java.util.Optional;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.bukkit.Bukkit;

import com.cronutils.model.Cron;
import com.cronutils.model.time.ExecutionTime;

import ua.i0xhex.srvrestart.Manager;
import ua.i0xhex.srvrestart.ServerRestart;
import ua.i0xhex.srvrestart.action.Action;

public class RestartManager extends Manager {
    
    private LocalDateTime nextRestartTime;
    private ScheduledExecutorService executor;
    
    public RestartManager(ServerRestart plugin) {
        super(plugin);
    }
    
    @Override
    public void onEnable() {
        nextSchedule();
    }
    
    @Override
    public void onDisable() {
        stopSchedule();
    }
    
    @Override
    public void onReload() {
        nextSchedule();
    }
    
    // getters
    
    public LocalDateTime getNextRestartTime() {
        return nextRestartTime;
    }
    
    // func
    
    public void schedule(long millisToRestart) {
        nextRestartTime = LocalDateTime.ofInstant(
                Instant.ofEpochMilli(System.currentTimeMillis() + millisToRestart),
                ZoneId.systemDefault());
    
        executor = Executors.newSingleThreadScheduledExecutor();
        plugin.config().getActionMap().forEach((seconds, actions) -> {
            long millisToTask = millisToRestart - seconds * 1000;
            if (millisToTask < 0) return;
            
            executor.schedule(() -> {
                runSync(() -> actions.forEach(Action::execute));
            }, millisToTask, TimeUnit.MILLISECONDS);
        });
        
        executor.schedule(() -> runSync(this::nextSchedule),
                millisToRestart, TimeUnit.MILLISECONDS);
    }
    
    public void scheduleSkipped() {
        stopSchedule();
        if (nextRestartTime != null) {
            ZonedDateTime timeToSkip = nextRestartTime
                    .atZone(ZoneId.systemDefault());
            nextRestartTime = null;
            getNextDelayMillis(timeToSkip).ifPresent(this::schedule);
        }
    }
    
    public void nextSchedule() {
        stopSchedule();
        nextRestartTime = null;
        getNextDelayMillis().ifPresent(this::schedule);
    }
    
    public void stopSchedule() {
        if (executor != null)
            executor.shutdownNow();
    }
    
    // internal
    
    private Optional<Long> getNextDelayMillis() {
        return getNextDelayMillis(ZonedDateTime.now());
    }
    
    private Optional<Long> getNextDelayMillis(ZonedDateTime timeAfter) {
        long currentMillis = System.currentTimeMillis();
        
        // select next nearest time
        long minDelayMillis = -1;
        for (Cron cron : plugin.config().getCronList()) {
            Optional<ZonedDateTime> time = ExecutionTime.forCron(cron).nextExecution(timeAfter);
            if (time.isPresent()) {
                long delayMillis = time.get().toInstant().toEpochMilli() - currentMillis;
                if (delayMillis < minDelayMillis || minDelayMillis == -1) minDelayMillis = delayMillis;
            }
        }
        
        return minDelayMillis == -1 ? Optional.empty() : Optional.of(minDelayMillis + 1);
    }
    
    private void runSync(Runnable runnable) {
        Bukkit.getScheduler().runTask(plugin, runnable);
    }
}
