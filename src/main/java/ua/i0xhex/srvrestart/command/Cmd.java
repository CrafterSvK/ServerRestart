package ua.i0xhex.srvrestart.command;

import java.util.List;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;

import ua.i0xhex.srvrestart.ServerRestart;

public abstract class Cmd implements CommandExecutor, TabCompleter {
    protected ServerRestart plugin;
    
    public Cmd(ServerRestart plugin) {
        this.plugin = plugin;
    }
    
    @Override
    public abstract boolean onCommand(CommandSender sender, Command command, String s, String[] args);
    
    @Override
    public abstract List<String> onTabComplete(CommandSender sender, Command command, String s, String[] args);
}
