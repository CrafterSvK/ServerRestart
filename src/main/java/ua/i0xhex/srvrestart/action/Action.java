package ua.i0xhex.srvrestart.action;

public abstract class Action {
    protected boolean valid;
    protected String data;
    
    public Action(String data) {
        this.valid = true;
        this.data = data;
    }
    
    public abstract void execute();
}
