package ua.i0xhex.srvrestart.action.actions;

import ua.i0xhex.srvrestart.action.Action;

public class ActionEmpty extends Action {
    public ActionEmpty(String data) {
        super(data);
    }
    
    @Override
    public void execute() {
        // nothing to execute
    }
}
