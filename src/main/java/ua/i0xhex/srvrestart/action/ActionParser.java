package ua.i0xhex.srvrestart.action;

import org.bukkit.Bukkit;

import ua.i0xhex.srvrestart.action.actions.*;

public class ActionParser {
    public static Action parseAction(String data) {
        try {
            String[] dataArr = data.split(" \\| ", 2);
            switch (dataArr[0].toLowerCase()) {
                case "chat":
                    return new ActionChat(dataArr[1]);
                case "command":
                    return new ActionCommand(dataArr[1]);
                case "sound":
                    return new ActionSound(dataArr[1]);
                case "title":
                    return new ActionTitle(dataArr[1]);
                default:
                    throw new IllegalArgumentException();
            }
        } catch (Exception ex) {
            Bukkit.getLogger().warning(String.format("" +
                    "[ServerRestart] Invalid action format: `%s`.",
                    data.replace('§', '&')));
            return new ActionEmpty("");
        }
    }
}
